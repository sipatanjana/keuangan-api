<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserGrup extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];
}
