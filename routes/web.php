<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function () {
    return \Illuminate\Support\Str::random(32);
});

//  param = opsional
$router->get('/get-budget[/{param}]', function ($param = null) {
    return 'Haloo ' . $param;
});

$router->group(['prefix' => 'admin'], function () use ($router) {
    // Route untuk menampilkan data user admin only
    $router->get('users', function () {
        return "Data User Admin";
    });
});
